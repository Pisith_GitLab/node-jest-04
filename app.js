const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routes = require('./src/routers/routes');

require('./src/helper/database');

app.use(bodyParser.json({limit: '200mb'}));
app.use(bodyParser.urlencoded({limit: '200mb', extended: true}));

app.use('/api', routes);

// app.get('/',(req, res) => {
//     res.status(200).send(`Welcome`);
// })

// app.get('/data', (req, res) => {
//     const data = {
//         id: req.body.id,
//         name: 'Pisith',
//         status: 'TEST'
//     }
//     res.status(200).json(data);
// })


module.exports = app;
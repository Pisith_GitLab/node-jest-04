const mongoose = require('mongoose');
const env = require('dotenv')
env.config()

mongoose.connect(process.env.DATABASE_URL+ '', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', () => {
    // console.log(`---Fail connect to ${process.env.DATABASE_URL}---`);
})
db.once('open', () => {
    // console.log(`---Success connect to ${process.env.DATABASE_URL}---`);
})

module.exports = mongoose;

const app = require(`../app`);
const controller = require('../src/controllers/user.controller');
const supertest = require(`supertest`);

// const mongoose = require('mongoose')
// const databaseName = 'studentdb'

// beforeAll(async () => {
//   const url = `mongodb://127.0.0.1/${databaseName}`
//   await mongoose.connect(url, { useNewUrlParser: true })
// })

test(`GET /`, (done) => {
    supertest(app)
        .get(`/api`)
        .expect(200, 'Welcome to my API');
    done();
})

describe(`testing /users get all users`, () => {
    it(`should return list of user and status ok`, (done) => {
        supertest(app)
        .get('/api/users')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    })
})


// describe('Testing / homepage endpoints', () => {
//     it (`GET /api return 200 and Welcome to my API`, async (done) => {
//         await supertest(app).get('/api')
//         .expect(200)
//         .expect('Welcome to my API');
//         done();
//     })
// })

